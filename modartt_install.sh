#!/usr/bin/env bash

if ! command -v 7za &> /dev/null
then
    echo "7za could not be found. Install e.g. on arch with: sudo pacman -S p7zip"
    exit 1
fi

ZIPFILE=$1
TEMPDIR=$(mktemp -d)

7za x -bd -o${TEMPDIR} $ZIPFILE &> /dev/null
INSTRUMENT=$(ls $TEMPDIR)
ARCHITECTURE=$(uname -m | tr _ -)bit

DESTINATION=~/.local/bin
mkdir -p $DESTINATION
cp -rut $DESTINATION "${TEMPDIR}/${INSTRUMENT}/${ARCHITECTURE}/${INSTRUMENT}" "${TEMPDIR}/${INSTRUMENT}/${ARCHITECTURE}/extra"

mkdir -p ~/.lv2
cp -rut ~/.lv2 "${TEMPDIR}/${INSTRUMENT}/${ARCHITECTURE}/${INSTRUMENT}.lv2"

rm -rf $TEMPDIR


# e.g.: Pianoteq 8 -> Pianoteq
INSTRUMENT_WITHOUT_VERSION=$(echo ${INSTRUMENT} | cut -d ' ' -f 1)
ICON_URL="https://www.modartt.com/images/logo/${INSTRUMENT_WITHOUT_MAJOR_VERSION}%20macOS%20app%20icon.svg"
ICON_PATH=~/.local/share/icons
ICON=${ICON_PATH}/${INSTRUMENT_WITHOUT_VERSION}.svg
mkdir -p ${ICON_PATH}
if [ ! -f ${ICON} ]; then
  wget -O ${ICON} ${ICON_URL} &> /dev/null
fi


mkdir -p ~/.local/share/applications
cat << EOF > ~/.local/share/applications/${INSTRUMENT_WITHOUT_VERSION}.desktop
[Desktop Entry]
Type=Application
Name=${INSTRUMENT}
Path=${DESTINATION}
Exec="${INSTRUMENT}"
Icon=${INSTRUMENT_WITHOUT_VERSION}.svg
Terminal=false
Categories=AudioVideo;Audio;Music;
EOF

